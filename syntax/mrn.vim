" Vim syntax file
" Language: Marain
" Maintainer: Joshua Wu
" Latest Revision: 6 January 2016
" Version: 1.1

if exists("b:current_syntax")
  finish
endif

syn region marainLogic matchgroup=marainDelimiter start='{' end='}'
  \ contains=marainAliasExpression,marainOperator,marainVariable,marainLiteral,marainKeyword
syn keyword marainRepeatKeyword for nextgroup=marainForVariables skipwhite contained containedin=marainLogic
syn match marainForVariables '\w\+\(\s*,\s*\w\+\)*\(\s\+in\)\@=' contained contains=marainForVariable,marainKeyword
syn match marainForVariable '\w\+' contained contains=marainKeyword
syn keyword marainRepeatKeyword in contained containedin=marainLogic
syn region marainForArguments matchgroup=marainDelimiter start='\(in\s\+\)\@<=\[' end=']' containedin=marainLogic transparent
  \ contains=marainKeyword
syn match marainEndLogic '{\@<=\s*end\s*}\@=' contained contains=marainKeyword containedin=marainLogic transparent

syn match marainNospaceOperator '\s\+\^?\?\s\+'
syn match marainNospaceOperator '(\@<=\^)@\='
syn match marainAliasExpression '\(\S\&[^|]\)\+|\(\S\&[^|]\)\+' contains=marainAlias,marainVariable,marainOperator,marainKeyword
syn match marainAlias '\(\S\&[^|]\)\+|\(\(\S\&[^|]\)\+\)\@=|\@!' contained
syn match marainOperator '|' contained containedin=marainAlias
syn match marainOperator '('
syn match marainOperator ')'
syn match marainOperator '?'
syn match marainOperator '='
syn match marainOperator '\S\@<=+\S\@='
syn match marainOperator '\S\@<=\*\w\@!'
syn match marainOperator '\w\@<!-\S\@<='
syn match marainDelimiter '\[' contained
syn match marainDelimiter ']' contained
syn match marainDelimiter ':'
syn match marainDelimiter ';'
syn match marainVariable '@\w\+' contains=marainKeyword
syn match marainVariable '\$\w\+' contains=marainKeyword
syn match marainProduction '\(^\s*\)\@<=\S\+\(\(([[:alnum:]_$@+,. ]\+)\)\?\(:\|;\)\s*\(#.*\)\?$\)\@='
  \ contains=marainAliasExpression,marainOperator,marainVariable,marainKeyword
syn match marainNumber '\w\@<!\d\+\(\.\d\+\)\?\w\@!'
syn match marainLiteral +'\([^'\\]\|\\.\)*'+
syn match marainLiteral '"\([^"\\]\|\\.\)*"'
syn match marainComment '#.*$' contains=marainTodo
syn match marainTodo 'TODO:.*' contained
syn match marainConstant '![[:alnum:].]\+'

syn match marainKeyword '[.@$]\@<!\<for\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<in\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<end\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<if\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<assert\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<include\>\.\@!'
syn match marainKeyword '[.@$]\@<!\<delimiter\>\.\@!'

"syn region marainLiteral start=+[uU]\=\z(['"]\)+ end="\z1" skip="\\\\\|\\\z1"

let b:current_syntax = "mrn"

hi def link marainProduction      Underlined
hi def link marainRepeatKeyword   Repeat
hi def link marainOperator        Operator
hi def link marainNospaceOperator Operator
hi def link marainKeyword         Keyword
hi def link marainDelimiter       Delimiter
hi def link marainAlias           Define
hi def link marainVariable        Identifier
hi def link marainForVariable     Identifier
hi def link marainConstant        StorageClass
hi def link marainComment         Comment
hi def link marainNumber          Number
hi def link marainLiteral         String
hi def link marainTodo            Todo
